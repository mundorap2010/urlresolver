import dryscrape
import sys
from bs4 import BeautifulSoup

def get_element_by_id_rapidvideo(body, id):
    soup = BeautifulSoup(body, 'html.parser')
    result = soup.find('source')
    return result['src']

def get_element_by_id_openload(body, id):
    soup = BeautifulSoup(body, 'html.parser')
    result = soup.find('p', attrs={ "id" : id})
    return result.text

def get_element_by_id_streamango(body, id):
    soup = BeautifulSoup(body, 'html.parser')
    result = soup.find('video', attrs={ "id" : id})
    return result['src']

url = sys.argv[1]
dryscrape.start_xvfb()
session = dryscrape.Session()
session.visit(url)
response = session.body()

if 'openload' in url:
    response = 'https://openload.co/stream/' + get_element_by_id_openload(response, 'DtsBlkVFQx')
    print(response)
elif 'rapidvideo' in url:
    response = get_element_by_id_rapidvideo(response, 'videojs_html5_api')
    print(response)
elif 'streamango' in url:
    response = get_element_by_id_streamango(response, 'mgvideo_html5_api')
    print('http:' + response)